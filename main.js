/* javascrip te datanın gösterimi için farklı yöntemler var örneğin
innerHTML
console.log()
document.write()
windows.alert()

var obje={
  firstname:"gülşah",
  lastname="gök"
};
*/
/* javascrip soldan sağa doğru okur örneğin solda string ile başlıyorsa sonradan gelen int ler string gibi davranır
var x="gülşah"+12+3;
sonucumuz=gülşah123 olarak çıkar ama 12+3 başta olsaydı matematiksel işlemi yapardı 15gülşah olurdu
*/


/*var sayi =3;
var ondakiksayi =6.9;
var text= "lyk17";
var kontrol=true;//false
var liste =[
  5,
  7.9,
  "javascript",
  true,
  ["gülşah","gök",5]
];
var sayi=6;
if(sayi==6){
  console.log("sayı 6 ya eşit");
}
var basamak = 12;
switch (basamak) {
  case 0:
  console.log("basamak=",basamak);
    break;
    case 1:
    console.log("basamak=",basamak);
    break;
    case 3:
    console.log("basamak=",basamak);
    break;
    case 4:
    console.log("basamak=",basamak);
    break;
    case 5:
    console.log("basamak=",basamak);
    break;
    default:
    console.log("basamak tanımlanamadı");

}
*/

/*fonksiyon tanımlarken bir fonskiyon ismi seçerek bunu çağırabiliiriz
function fonksiyon(){
  console.log("gülşah javascript öğreniyor");
}
*/
/* burada bir değer ataması yapmış ve sayi parametrelesi yerine deger dönmüş sonuc 8+3 ten 11 etmiştir.

var deger=8;
function fonksiyon(sayi){
  return sayi+3;
}
var sonuc=fonksiyon(deger);
console.log(sonuc);
*/
/*
var dizi=[
  "gülşah","gök","123"
];
document.getElementById("demo").innerHTML=dizi[0];
*/
/*
var ogrenci = {
  firstname:"gülşah",
  lastname:"gök",
  age:21,
  eyecolor:"black"
};
document.getElementById("demo").innerHTML=
ogrenci.firstname+ogrenci.lastname+"is"+ogrenci.age+"years old"+ogrenci.eyecolor;
*/
//typeof bir değişkenin ya da ifadenin türünü döndürür
/*
document.getElementById("demo").innerHTML=
typeof " " + "<br>"+
typeof "gülşah" + "<br>"+
typeof 34 +"<br>";
*/
/*
var sayi=2;
function islem(){
  if(sayi%2==0){
    return "çift";
  }else{
    return "tek";
  }
}
var sonuc=islem(sayi);
console.log(sonuc);
*/
/*function dongu(){
  var giris=prompt("sayı giriniz");
  var sayi=parseInt(giris);
  for(i=sayi;sayi>=0;i--){
  console.log(i*sayi);
  }
}
dongu();
*/
/*
var dizi=[
  0,
  2,
  4,
  6,
  8,
  10,
  12
];
for(var i=0;i<dizi.length;i++){
  console.log(dizi[i]);
}
*/
//for each fonksiyonu bir dizi alıcak bir fonksiyon dizideki her bir elemanı alacak for la olacak
